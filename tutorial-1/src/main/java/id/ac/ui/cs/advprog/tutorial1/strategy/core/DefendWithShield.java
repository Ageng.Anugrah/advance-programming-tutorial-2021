package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithShield() {}

    public String defend() {
        return "Summon Marvel's Agents of S.H.I.E.L.D";
    }

    public String getType() {
        return "Defend With Shield";
    }
}
