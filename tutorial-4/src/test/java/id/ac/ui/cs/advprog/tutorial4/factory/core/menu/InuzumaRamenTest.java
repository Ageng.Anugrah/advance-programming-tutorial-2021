package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class InuzumaRamenTest {

    private Class<?> menuClass;

    @BeforeEach
    public void setUp() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMenuName() throws Exception {
        InuzumaRamen inuzumaRamen = new InuzumaRamen("Dummy");
        assertEquals("Dummy", inuzumaRamen.getName());
    }

    @Test
    public void testToppingDescription() throws Exception {
        InuzumaRamen inuzumaRamen = new InuzumaRamen("Dummy");
        assertEquals("Adding Guahuan Boiled Egg Topping", inuzumaRamen.getTopping().getDescription());
    }

    @Test
    public void testMeatDescription() throws Exception {
        InuzumaRamen inuzumaRamen = new InuzumaRamen("Dummy");
        assertEquals("Adding Tian Xu Pork Meat...", inuzumaRamen.getMeat().getDescription());
    }

    @Test
    public void testNoodleDescription() throws Exception {
        InuzumaRamen inuzumaRamen = new InuzumaRamen("Dummy");
        assertEquals("Adding Inuzuma Ramen Noodles...", inuzumaRamen.getNoodle().getDescription());
    }

    @Test
    public void testFlavorDescription() throws Exception {
        InuzumaRamen inuzumaRamen = new InuzumaRamen("Dummy");
        assertEquals("Adding Liyuan Chili Powder...", inuzumaRamen.getFlavor().getDescription());
    }

}
