package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RamenTest {

    private Class<?> generalNoodleClass;

    @BeforeEach
    public void setUp() throws Exception {
        generalNoodleClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle");
    }

    @Test
    public void testPorkDescription() throws Exception {
        Ramen ramen = new Ramen();
        assertEquals("Adding Inuzuma Ramen Noodles...", ramen.getDescription());
    }

}
