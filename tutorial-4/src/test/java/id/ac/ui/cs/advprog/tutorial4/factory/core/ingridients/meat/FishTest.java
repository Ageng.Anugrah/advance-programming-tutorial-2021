package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class FishTest {

    private Class<?> fishClass;

    @BeforeEach
    public void setUp() throws Exception {
        fishClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat");
    }

    @Test
    public void testFishDescription() throws Exception {
        Fish fish = new Fish();
        assertEquals("Adding Zhangyun Salmon Fish Meat...", fish.getDescription());
    }

}
