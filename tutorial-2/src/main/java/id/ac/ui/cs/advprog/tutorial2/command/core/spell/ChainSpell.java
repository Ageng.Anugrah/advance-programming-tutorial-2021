package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }

    @Override
    public void cast() {
        for (Spell spell : spells){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = this.spells.size()-1; i >= 0; i--) {
            Spell spell = this.spells.get(i);
            spell.undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
