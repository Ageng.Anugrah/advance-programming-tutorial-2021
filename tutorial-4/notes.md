### Eager Instantiation vs Lazy Instantiation
___

__Eager Instantiation__

Dengan menggunakan Eager Instantiation, objek harus dibuat terlebih dahulu dan harus siap digunakan ketika dipaggil. Kita tahu bahwa sebelum menjalankan aplikasi, semua kelas dimuat terlebih dahulu. Dan saat memuat kelas, semua anggota datanya dibuat instance-nya. Jadi, ketika kita akan menjalankan aplikasi kita, kelas Singleton juga akan dimuat dan anggota data juga akan dibuat.

Karena objek ini akan dibuat instance-nya terlebih dahulu, jadi tidak peduli apakah kita akan menggunakannya atau tidak, objek ini sudah  tersedia dan siap digunakan (Eagerly Instantiated).

Untuk kelebihan Eiger Instantiation adalah jika memang objek tersebut akan digunakan dalam banyak method atau class, objek tersebut sudah dibuat saat program berjalan sehingga objek siap dihunakan kapanpun dan dapat menghemat waktu juga. Kekurangannya adalah jika tidak digunakan dengan tepat maka resource akan terbuang sia-sia baik waktu maupun memory apabila instance tersebut tidak digunakan setiap saat.

__Lazy Instantiation__

Dengan menggunakan Lazy Instantiation objek akan dibuat instance-nya ketika suatu fungsi atau class memanggil method getInstance(). 
Ini akan memeriksa apakah objek null (berarti pemanggilan pertama kali metode getInstance()) atau tidak, kemudian jika null objek akan dibuat dan akan direturn. 
Tetapi tidak null, itu berarti instance Singleton sudah diinisiasi oleh, jadi itu hanya akan mengembalikan instance reference Singleton yang sudah diinisialisasi.

Kelebihan dari Lazy Instantiation adalah kita bisa menyimpan resource (baik memori maupun waktu) apabila objek Singleton tersebut sifatnya berat. Namun kekurangannya adalah ketika kita memanggil getInstance() kondisi null akan selalu diperiksa, sehingga jika ada banyak fungsi yang memanggil getInstance() program akan bekerja lebih berat. Selain itu dalam multithread environtment, bisa saja merusak property singleton.
