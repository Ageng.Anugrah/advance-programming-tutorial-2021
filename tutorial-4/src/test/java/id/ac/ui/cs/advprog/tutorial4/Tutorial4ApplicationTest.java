package id.ac.ui.cs.advprog.tutorial4;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Tutorial4ApplicationTest {
    @Test
    void contextLoads() {

    }

    @Test
    public void testMain() {
        Tutorial4Application.main(new String[] {});
    }

}
