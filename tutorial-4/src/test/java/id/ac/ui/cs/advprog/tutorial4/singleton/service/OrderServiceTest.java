package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @Mock
    OrderDrink orderDrink;

    @Mock
    OrderFood orderFood;

    @InjectMocks
    private OrderServiceImpl orderService;

    @Test
    public void testOrderADrinkImplementedCorrectly() throws Exception {
        orderService = new OrderServiceImpl();
        orderService.orderADrink("Dummy");
        String drinkName = orderService.getDrink().getDrink();
        assertEquals("Dummy", drinkName);
    }

    @Test
    public void testOrderAFoodImplementedCorrectly() throws Exception {
        orderService = new OrderServiceImpl();
        orderService.orderAFood("Dummy");
        String foodName = orderService.getFood().getFood();
        assertEquals("Dummy", foodName);
    }
}

