package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class AtbashTransformation {

    public Spell encode (Spell spell){
        return process(spell, true);
    }

    public Spell decode (Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode){
        String text = spell.getText();
        String result = "";
        Codex codex = spell.getCodex();
        for (int i = 0; i < text.length(); i++) {
            int ascii = (int) text.charAt(i);
            int asciiAfter;
            if (ascii >= 97) {
                asciiAfter = 219 - ascii;
            } else if (ascii >= 65) {
                asciiAfter = 155 - ascii;
            } else if (ascii >= 48) {
                asciiAfter = 105 - ascii;
            } else {
                asciiAfter = ascii;
            }
            char ch = (char) asciiAfter;
            result += ch;
        }

        return new Spell(result, codex);
    }
}
