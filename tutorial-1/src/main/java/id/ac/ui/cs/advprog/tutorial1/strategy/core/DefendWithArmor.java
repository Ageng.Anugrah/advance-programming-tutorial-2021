package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    public DefendWithArmor() {}

    public String defend() {
        return "Kuchiyose Armored Titan!!";
    }

    public String getType() {
        return "Defend With Armor";
    }
}
