package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UmamiTest {

    private Class<?> umamiClass;

    @BeforeEach
    public void setUp() throws Exception {
        umamiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat");
    }

    @Test
    public void testUmamiDescription() throws Exception {
        Umami umami = new Umami();
        assertEquals("Adding WanPlus Specialty MSG flavoring...", umami.getDescription());
    }

}
