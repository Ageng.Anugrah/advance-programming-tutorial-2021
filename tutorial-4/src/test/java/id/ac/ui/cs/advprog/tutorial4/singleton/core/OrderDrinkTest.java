package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderDrinkTest {
    private OrderDrink orderDrink;

    @Test
    public void testOrderDrinkOnlyOneInstance() {
        OrderDrink orderDrink1 = orderDrink.getInstance();
        OrderDrink orderDrink2 = orderDrink.getInstance();

        assertThat(orderDrink1).isEqualToComparingFieldByField(orderDrink2);
    }

}

