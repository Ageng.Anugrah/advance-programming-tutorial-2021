package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderFoodTest {
    private OrderFood orderFood;

    @Test
    public void testOrderFoodOnlyOneInstance() {
        OrderFood orderFood1 = orderFood.getInstance();
        OrderFood orderFood2 = orderFood.getInstance();

        assertThat(orderFood1).isEqualToComparingFieldByField(orderFood2);
    }
}


